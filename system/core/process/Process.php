<?php

namespace Ionscript\Process;


class Process
{
    private $pid;
    private $command;

    public function __construct($cl = false)
    {
        if ($cl !== false) {
            $this->command = $cl;
            $this->runCom();
        }
    }

    private function runCom()
    {
        $command = 'nohup ' . $this->command . ' > /dev/null 2>&1 & echo $!';
        exec($command, $op);
        $this->pid = (int)$op[0];

        return null;
    }

    public function setPid($pid)
    {
        $this->pid = $pid;
    }

    public function getPid()
    {
        return $this->pid;
    }

    public function status()
    {
        $command = 'ps -p ' . $this->pid;
        exec($command, $op);
        return isset($op[1]);
    }

    public function start()
    {
        return ($this->command !== '') ?: $this->runCom();
    }

    public function stop()
    {
        $command = 'kill ' . $this->pid;
        exec($command);
        return $this->status();
    }
}


$process = new Process('php --ini');
//var_dump($process->status());
//
////$process->stop();
$process->start();
if ($process->status()){
    echo "The process is currently running";
}else{
    echo "The process is not running.";
}
$pr = '';