<?php

/**
 *
 * URI         = scheme ":" hier-part [ "?" query ] [ "#" fragment ]
 * hier-part   = "//" authority path-abempty
 * / path-absolute
 * / path-rootless
 * / path-empty 
 * 
 */

/**
 * 
 *   foo://example.com:8080/over/there?name=values#noise
 *   \_/   \______________/\_________/ \_________/ \__/
 *    |           |            |            |        |
 * scheme     authority       path        query   fragment
 *    |   _____________________|__
 *   / \ /                        \
 *   urn:example:animal:ferret:nose
 * 
  */


interface UriInterface
{
    const CHAR_UNRESERVED   = 'a-zA-Z0-9_\-\.~';
    const CHAR_GEN_DELIMS   = ':\/\?#\[\]@';
    const CHAR_SUB_DELIMS   = '!\$&\'\(\)\*\+,;=';
    const CHAR_RESERVED     = ':\/\?#\[\]@!\$&\'\(\)\*\+,;=';
    const CHAR_QUERY_DELIMS = '!\$\'\(\)\*\,';
    
    public function getScheme();

    public function getUserInfo();

    public function getHost();

    public function getPort();

    public function getPath();

    public function getQuery();

    public function getFragment();

    public function setScheme($scheme);

    public function setUserInfo($userInfo);

    public function setHost($host);

    public function setPort($port);

    public function setPath($path);

    public function setQuery($query);

    public function setFragment($fragment);

    public function __toString();
}
