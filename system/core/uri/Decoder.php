<?php


abstract class Decoder
{
    use Util;
    
    public static function decodeUrl($input, $allowed = '')
    {
        return preg_replace_callback(
            '/%[A-Fa-f0-9]{2}/',
            function ($match) use ($allowed) {
                $char = rawurldecode($match[0]);
                if (preg_match($allowed, $char)) {
                    return $char;
                }
                return strtoupper($match[0]);
            },
            $input
        );
    }
}
