<?php

class Uri implements UriInterface
{
    protected $scheme;
    protected $userInfo;
    protected $host;
    protected $port;
    protected $path;
    protected $query;
    protected $fragment;

    public function __construct($uri = '')
    {
        $uri ? $this->parse($uri) : null;
    }
    
    public function isValid()
    {
        if ($this->host) {
            if ($this->path && $this->path[0] !== '/') {
                return false;
            }
            return true;
        }

        if ($this->userInfo || $this->port) {
            return false;
        }

        if ($this->path) {
            if (0 === strpos($this->path, '//')) {
                return false;
            }
            return true;
        }

        if (! ($this->query || $this->fragment)) {
            return false;
        }

        return true;
    }

    protected function reset()
    {
        $this->scheme = null;
        $this->port = null;
        $this->userInfo = null;
        $this->host = null;
        $this->path = null;
        $this->fragment = null;
        $this->query = null;
    }

    public function parse($uri)
    {
        $this->reset();

        if (($scheme = self::parseScheme($uri)) !== null) {
            $this->setScheme($scheme);
            $uri = substr($uri, strlen($scheme) + 1) ?: '';
        }

        if (preg_match('|^//([^/\?#]*)|', $uri, $match)) {
            $authority = $match[1];
            $uri       = substr($uri, strlen($match[0]));

            if (strpos($authority, '@') !== false) {
                $segments  = explode('@', $authority);
                $authority = array_pop($segments);
                $userInfo  = implode('@', $segments);
                unset($segments);
                $this->setUserInfo($userInfo);
            }

            $nMatches = preg_match('/:[\d]{1,5}$/', $authority, $matches);
            if ($nMatches === 1) {
                $portLength = strlen($matches[0]);
                $port = substr($matches[0], 1);

                $this->setPort((int) $port);
                $authority = substr($authority, 0, -$portLength);
            }

            $this->setHost($authority);
        }

        if (!$uri) {
            return $this;
        }

        if (preg_match('|^[^\?#]*|', $uri, $match)) {
            $this->setPath($match[0]);
            $uri = substr($uri, strlen($match[0]));
        }

        if (!$uri) {
            return $this;
        }

        if (preg_match('|^\?([^#]*)|', $uri, $match)) {
            $this->setQuery($match[1]);
            $uri = substr($uri, strlen($match[0]));
        }
        if (!$uri) {
            return $this;
        }

        if ($uri && $uri[0] === '#') {
            $this->setFragment(substr($uri, 1));
        }

        return $this;
    }

    protected static function parseScheme($uriString)
    {
        if (! is_string($uriString)) {
            throw new \InvalidArgumentException(sprintf(
                'Expecting a string, got %s',
                (is_object($uriString) ? get_class($uriString) : gettype($uriString))
            ));
        }

        if (preg_match('/^([A-Za-z][A-Za-z0-9\.\+\-]*):/', $uriString, $match)) {
            return $match[1];
        }

        return null;
    }
    
    public function toString()
    {
        if (!$this->isValid()) {
                throw new \InvalidArgumentException(
                    'URI is not valid and cannot be converted into a string'
                );
        }

        $uri = '';

        if ($this->scheme) {
            $uri .= $this->scheme . ':';
        }

        if ($this->host !== null) {
            $uri .= '//';
            if ($this->userInfo) {
                $uri .= $this->userInfo . '@';
            }
            $uri .= $this->host;
            if ($this->port) {
                $uri .= ':' . $this->port;
            }
        }

        if ($this->path) {
            $uri .= Encoder::encodePath($this->path);
        } elseif ($this->host && ($this->query || $this->fragment)) {
            $uri .= '/';
        }

        if ($this->query) {
            $uri .= "?" . Encoder::encodeQueryFragment($this->query);
        }

        if ($this->fragment) {
            $uri .= "#" . Encoder::encodeQueryFragment($this->fragment);
        }

        return $uri;
    }

    public function normalize()
    {
        if ($this->scheme) {
            $this->scheme = Normalizer::normalizeScheme($this->scheme);
        }

        if ($this->host) {
            $this->host = Normalizer::normalizeHost($this->host);
        }

        if ($this->port) {
            $this->port = Normalizer::normalizePort($this->port, $this->scheme);
        }

        if ($this->path) {
            $this->path = Normalizer::normalizePath($this->path);
        }

        if ($this->query) {
            $this->query = Normalizer::normalizeQuery($this->query);
        }

        if ($this->fragment) {
            $this->fragment = Normalizer::normalizeFragment($this->fragment);
        }
        
        if ($this->host && empty($this->path)) {
            $this->path = '/';
        }

        return $this;
    }

//    public function create($baseUri)
//    {
//        if (is_string($baseUri)) {
//            $baseUri = new static($baseUri);
//        } elseif (!$baseUri instanceof Uri) {
//            throw new \InvalidArgumentException(
//                'Provided base URI must be a string or a Uri object'
//            );
//        }
//
//        if ($this->getHost()) {
//            $this->setPath(Normalizer::removePathDotSegments($this->getPath()));
//        } else {
//            $basePath = $baseUri->getPath();
//            $relPath  = $this->getPath();
//            if (!$relPath) {
//                $this->setPath($basePath);
//                if (!$this->getQuery()) {
//                    $this->setQuery($baseUri->getQuery());
//                }
//            } else {
//                if ($relPath[0] === '/') {
//                    $this->setPath(Normalizer::removePathDotSegments($relPath));
//                } else {
//                    if ($baseUri->getHost() && !$basePath) {
//                        $mergedPath = '/';
//                    } else {
//                        $mergedPath = substr($basePath, 0, strrpos($basePath, '/') + 1);
//                    }
//                    $this->setPath(Normalizer::removePathDotSegments($mergedPath . $relPath));
//                }
//            }
//
//            $this->setUserInfo($baseUri->getUserInfo());
//            $this->setHost($baseUri->getHost());
//            $this->setPort($baseUri->getPort());
//        }
//
//        $this->setScheme($baseUri->getScheme());
//        return $this;
//    }

    public function getScheme()
    {
        return $this->scheme;
    }

    public function getUserInfo()
    {
        return $this->userInfo;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getQueryAsArray()
    {
        $query = [];
        if ($this->query) {
            parse_str($this->query, $query);
        }

        return $query;
    }

    public function getFragment()
    {
        return $this->fragment;
    }

    public function setScheme($scheme)
    {
        if (!Validator::validateScheme($scheme)) {
            throw new \InvalidArgumentException(sprintf(
                'Scheme "%s" is not valid or is not accepted by %s',
                $scheme,
                get_class($this)
            ));
        }

        $this->scheme = $scheme;
        return $this;
    }

    public function setUserInfo($userUnfo)
    {
        $this->userInfo = $userUnfo;
        return $this;
    }
    
    public function setHost($host)
    {
        if (($host !== '')
            && ($host !== null)
            && !Validator::validateHostname($host)
        ) {
            throw new \InvalidArgumentException(sprintf(
                'Host "%s" is not valid or is not accepted by %s',
                $host,
                get_class($this)
            ));
        }

        $this->host = $host;
        return $this;
    }
    
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }
    
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }
    
    public function setQuery($query)
    {
        if (is_array($query)) {
            $query = str_replace('+', '%20', http_build_query($query));
        }

        $this->query = $query;
        return $this;
    }
    
    public function setFragment($fragment)
    {
        $this->fragment = $fragment;
        return $this;
    }
    
    public function __toString()
    {
        try {
            return $this->toString();
        } catch (\Exception $e) {
            return '';
        }
    }
}
