<?php


abstract class Validator
{
    use Util;
    
    public static function validateScheme($scheme)
    {
        return (bool) preg_match('/^[A-Za-z][A-Za-z0-9\-\.+]*$/', $scheme);
    }

    public static function validateUserInfo($userInfo)
    {
        return (bool) preg_match('/^(?:[' . URI::CHAR_UNRESERVED . URI::CHAR_SUB_DELIMS . ':]+|%[A-Fa-f0-9]{2})*$/', $userInfo);
    }

    public static function validatePort($port)
    {
        if ($port === 0) {
            return false;
        }

        if ($port) {
            $port = (int) $port;
            if ($port < 1 || $port > 0xffff) {
                return false;
            }
        }

        return true;
    }

    public static function validatePath($path)
    {
        $pchar   = '(?:[' . URI::CHAR_UNRESERVED . ':@&=\+\$,]+|%[A-Fa-f0-9]{2})*';
        $segment = $pchar . "(?:;{$pchar})*";
        return (bool) preg_match("/^{$segment}(?:\/{$segment})*$/", $path);
    }

    public static function validateQueryFragment($input)
    {
        return (bool) preg_match('/^(?:[' . URI::CHAR_UNRESERVED . URI::CHAR_SUB_DELIMS . ':@\/\?]+|%[A-Fa-f0-9]{2})*$/', $input);
    }

    public static function validateIpAddress($host)
    {
        return (preg_match('/^\[.*\]$/', $host));
    }

    public static function validateHostname($host)
    {
        return (bool) preg_match('/^(?:[' . URI::CHAR_UNRESERVED . URI::CHAR_SUB_DELIMS . ':@\/\?]+|%[A-Fa-f0-9]{2})+$/', $host);
    }
}
