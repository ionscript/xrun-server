<?php


abstract class Encoder
{
    use Util;
    
    protected static $encoding = 'utf-8';
    
    public static $supportedEncodings = array(
        'iso-8859-1',   'iso8859-1',    'iso-8859-5',   'iso8859-5',
        'iso-8859-15',  'iso8859-15',   'utf-8',        'cp866',
        'ibm866',       '866',          'cp1251',       'windows-1251',
        'win-1251',     '1251',         'cp1252',       'windows-1252',
        '1252',         'koi8-r',       'koi8-ru',      'koi8r',
        'big5',         '950',          'gb2312',       '936',
        'big5-hkscs',   'shift_jis',    'sjis',         'sjis-win',
        'cp932',        '932',          'euc-jp',       'eucjp',
        'eucjp-win',    'macroman'
    );
    
    public static function getEncoding()
    {
        return self::$encoding;
    }
    
    public static function convertEncoding($string, $to, $from)
    {
        if (function_exists('iconv')) {
            $result = iconv($from, $to, $string);
        } elseif (function_exists('mb_convert_encoding')) {
            $result = mb_convert_encoding($string, $to, $from);
        } else {
            throw new \RuntimeException(
                __CLASS__
                . ' requires either the iconv or mbstring extension to be installed'
                . ' when escaping for non UTF-8 strings.'
            );
        }

        if ($result === false) {
            return ''; // return non-fatal blank string on encoding errors from users
        }
        return $result;
    }
    
    public static function encodeUserInfo($userInfo)
    {
        if (!is_string($userInfo)) {
            throw new \InvalidArgumentException(sprintf(
                'Expecting a string, got %s',
                (is_object($userInfo) ? get_class($userInfo) : gettype($userInfo))
            ));
        }

        $regex   = '/(?:[^' . URI::CHAR_UNRESERVED . URI::CHAR_SUB_DELIMS . '%:]|%(?![A-Fa-f0-9]{2}))/';
        
        /** @var Escaper $escaper */
        $escaper = Escaper::get();
        $replace = function ($match) use ($escaper) {
            return $escaper->escapeUrl($match[0]);
        };

        return preg_replace_callback($regex, $replace, $userInfo);
    }

    public static function encodePath($path)
    {
        if (!is_string($path)) {
            throw new \InvalidArgumentException(sprintf(
                'Expecting a string, got %s',
                (is_object($path) ? get_class($path) : gettype($path))
            ));
        }

        $regex   = '/(?:[^' . URI::CHAR_UNRESERVED . ':@&=\+\$,\/;%]+|%(?![A-Fa-f0-9]{2}))/';

        /** @var Escaper $escaper */
        $escaper = Escaper::get();
        $replace = function ($match) use ($escaper) {
            return $escaper->escapeUrl($match[0]);
        };

        return preg_replace_callback($regex, $replace, $path);
    }
    
    public static function encodeQueryFragment($input)
    {
        if (!is_string($input)) {
            throw new \InvalidArgumentException(sprintf(
                'Expecting a string, got %s',
                (is_object($input) ? get_class($input) : gettype($input))
            ));
        }

        $regex   = '/(?:[^' . URI::CHAR_UNRESERVED . URI::CHAR_SUB_DELIMS . '%:@\/\?]+|%(?![A-Fa-f0-9]{2}))/';

        /** @var Escaper $escaper */
        $escaper = Escaper::get();
        $replace = function ($match) use ($escaper) {
            return $escaper->escapeUrl($match[0]);
        };

        return preg_replace_callback($regex, $replace, $input);
    }
}
