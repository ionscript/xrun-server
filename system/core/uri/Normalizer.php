<?php


abstract class Normalizer
{
    use Util;
    
    protected static $defaultPorts = [];
    
    public static function normalizeScheme($scheme)
    {
        return strtolower($scheme);
    }

    public static function normalizeHost($host)
    {
        return strtolower($host);
    }

    public static function normalizePort($port, $scheme = null)
    {
        if ($scheme && isset(self::$defaultPorts[$scheme]) && ($port == self::$defaultPorts[$scheme])) {
            return null;
        }

        return $port;
    }

    public static function normalizePath($path)
    {
        $path = Encoder::encodePath(
            Decoder::decodeUrl(
                self::removePathDotSegments($path),
                '/[' . URI::CHAR_UNRESERVED . ':@&=\+\$,\/;%]/'
            )
        );

        return $path;
    }

    public static function normalizeQuery($query)
    {
        $query = Encoder::encodeQueryFragment(
            Decoder::decodeUrl(
                $query,
                '/[' . URI::CHAR_UNRESERVED . URI::CHAR_QUERY_DELIMS . ':@\/\?]/'
            )
        );

        return $query;
    }

    public static function normalizeFragment($fragment)
    {
        $fragment = Encoder::encodeQueryFragment(
            Decoder::decodeUrl(
                $fragment,
                '/[' . URI::CHAR_UNRESERVED . URI::CHAR_SUB_DELIMS . '%:@\/\?]/'
            )
        );

        return $fragment;
    }
    
    public static function removePathDotSegments($path)
    {
        $output = '';

        while ($path) {
            if ($path === '..' || $path === '.') {
                break;
            }

            switch (true) {
                case ($path === '/.'):
                    $path = '/';
                    break;
                case ($path === '/..'):
                    $path   = '/';
                    $lastSlashPos = strrpos($output, '/', -1);
                    if (false === $lastSlashPos) {
                        break;
                    }
                    $output = substr($output, 0, $lastSlashPos);
                    break;
                case (0 === strpos($path, '/../')):
                    $path   = '/' . substr($path, 4);
                    $lastSlashPos = strrpos($output, '/', -1);
                    if (false === $lastSlashPos) {
                        break;
                    }
                    $output = substr($output, 0, $lastSlashPos);
                    break;
                case (0 === strpos($path, '/./')):
                    $path = substr($path, 2);
                    break;
                case (0 === strpos($path, './')):
                    $path = substr($path, 2);
                    break;
                case (0 === strpos($path, '../')):
                    $path = substr($path, 3);
                    break;
                default:
                    $slash = strpos($path, '/', 1);
                    if ($slash === false) {
                        $seg = $path;
                    } else {
                        $seg = substr($path, 0, $slash);
                    }

                    $output .= $seg;
                    $path    = substr($path, strlen($seg));
                    break;
            }
        }

        return $output;
    }
}
