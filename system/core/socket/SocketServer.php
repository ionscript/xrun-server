<?php

include_once 'Buffer.php';

abstract class SocketServer
{
    protected static $sock;
    protected static $conn;

    protected static $host;
    protected static $addr;
    protected static $port;

    protected static $remoteaddr;
    protected static $remoteport;

    protected static $protocol;

    protected function listen()
    {
        return socket_bind(static::$sock, static::$addr, static::$port) | socket_listen(static::$sock);
    }

    protected function create()
    {
        static::$addr = gethostbyname(static::$host);

        if ($socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) {
            static::$sock = $socket;
        }
    }

    protected function setOption($socket, $name, $value)
    {
        return !socket_set_option($socket, getprotobyname(static::$protocol), $name, $value) ?: $this;
    }

    protected function getOption($socket, $name)
    {
        return socket_get_option($socket, getprotobyname(static::$protocol), $name);
    }

    protected function sockname()
    {
        return socket_getsockname(static::$sock, static::$addr, static::$port);
    }

    protected function peername()
    {
        return socket_getpeername(static::$conn, static::$remoteaddr, static::$remoteport);
    }

    protected function connection()
    {
        return (false === (static::$conn = socket_accept(static::$sock))) ?: $this->peername();
    }

    protected function read()
    {
        if (false !== ($msg = socket_read(static::$conn, 2048))) {
            Buffer::$buffer = $msg;
            return strlen($msg);
        }

        return false;
    }

    protected function receive()
    {
        if (false !== ($bytes = socket_recv(static::$conn, Buffer::$buffer, 2048, 0))) {
            return $bytes;
        }

        return false;
    }

    protected function write()
    {
        if (false !== ($sent = socket_write(static::$conn, Buffer::$buffer, strlen(Buffer::$buffer)))) {
            return $sent;
        }

        return false;
    }

    protected function block($socket, $block = true)
    {
        return $block ? socket_set_block($socket) : socket_set_nonblock($socket);
    }

    protected function closeSock()
    {
        if (is_resource(static::$sock)) {
            socket_close(static::$sock);
        }

        static::$sock = null;
    }

    protected function closeConn()
    {
        if (is_resource(static::$conn)) {
            socket_close(static::$conn);
        }

        static::$conn = null;
    }
}
