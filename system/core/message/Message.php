<?php

class Message implements MessageInterface
{
    protected $metadata = [];
    protected $content = '';

    public function setContent($string)
    {
        if(!is_string($string)){
            throw new \InvalidArgumentException('Content expected a string.');
        }

        $this->content = $string;
        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getMetadata()
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata)
    {
        $this->metadata = $metadata;
    }
}
