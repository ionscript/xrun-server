<?php


interface MessageInterface
{
    public function setContent($value);

    public function getContent();
    
    public function getMetadata();
    
    public function setMetadata(array $metadata);
}
