<?php


class HttpRequest extends HttpMessage implements HttpRequestInterface
{
    protected $protocol;
    protected $method;
    protected $uri;
    protected $query;

    public static function create($string)
    {
        $request = new static;

        $lines = array_filter(
            explode("\r\n", $string),
            function ($value) {
                return (bool)$value;
            });

        $matches = null;

        $regex = '#^(?P<method>' . self::METHODS . ')\s(?P<uri>[^ ]*)(?:\s(?P<protocol>' . self::PROTOCOLS . ')\/(?P<version>\d+\.\d+)){0,1}#';
        $firstLine = array_shift($lines);

        if (!preg_match($regex, $firstLine, $matches)) {
            throw new \RuntimeException('A valid request line was not found in the provided string');
        }

        $request->setMethod($matches['method']);
        $request->setProtocol($matches['protocol']);

        if ($lines) {
            $isHeader = true;

            $headers = $server = $rawBody = [];
            while ($lines) {
                $nextLine = array_shift($lines);
                if ($nextLine === '') {
                    $isHeader = false;
                    continue;
                }

                if ($isHeader) {
                    if (preg_match("/[\r\n]/", $nextLine)) {
                        throw new \RuntimeException('CRLF injection detected');
                    }

                    $header = explode(':', $nextLine, 2);
                    $headers[$header[0]] = trim($header[1]);
                    continue;
                }

                if ($rawBody && preg_match('/^[a-z0-9!#$%&\'*+.^_`|~-]+:$/i', $nextLine)) {
                    throw new \RuntimeException('CRLF injection detected');
                }

                $rawBody[] = $nextLine;
            }

            if ($headers) {
                $request->setHeaders($headers);
            }

            if ($rawBody) {
                $request->setContent(implode("\r\n", $rawBody));
            }

            $request->setUri(strtolower(
                $request->getProtocol() . '://' . $request->getHeaders()['Host'] . $matches['uri']
            ));

            $parsedUri = parse_url($matches['uri']);

            if (array_key_exists('query', $parsedUri)) {
                $parsedQuery = [];
                parse_str($parsedUri['query'], $parsedQuery);
                $request->setQuery($parsedQuery);
            }
        }

        return $request;
    }

    public function getProtocol()
    {
        return $this->protocol;
    }

    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;
    }

    public function setMethod($method)
    {
        $method = strtoupper($method);
        if (strpos(self::METHODS, $method) === false) {
            throw new \InvalidArgumentException('Invalid HTTP method passed');
        }
        $this->method = $method;
        return $this;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setUri($uri)
    {
        if (is_string($uri)) {
            try {
                $uri = new Uri($uri);
            } catch (\InvalidArgumentException $e) {
                throw new \InvalidArgumentException(
                    sprintf('Invalid URI passed as string (%s)', (string)$uri),
                    $e->getCode(),
                    $e
                );
            }
        } elseif (!($uri instanceof Uri)) {
            throw new \InvalidArgumentException(
                'URI must be an instance of Uri or a string'
            );
        }

        $this->uri = $uri;

        return $this;
    }

    public function getUri()
    {
        if ($this->uri === null || is_string($this->uri)) {
            $this->uri = new Uri($this->uri);
        }
        return $this->uri;
    }

    public function getUriString()
    {
        if ($this->uri instanceof Uri) {
            return $this->uri->toString();
        }
        return $this->uri;
    }

    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    public function getQuery($name = null)
    {
        if ($name === null) {
            return $this->query;
        }

        return $this->query[$name];
    }
}
