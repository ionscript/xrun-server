<?php

interface HttpRequestInterface
{
    const PROTOCOLS = 'HTTP|HTTPS';
    const METHODS = 'GET|POST|PUT|PATCH|DELETE|HEAD|OPTIONS';

    public function getMethod();

    public function setMethod($method);

    public function getUri();

    public function setUri($uri);
}
