<?php

interface HttpServerInterface
{
    //$command = 'nohup ' . $this->command . ' > /dev/null 2>&1 & echo $!';
    //const EXECUTE = 'echo \'%s\' 2>/dev/null | /usr/bin/php -f \'%s\'';
    const EXECUTE = 'echo \'%s\' 2>/dev/null | /usr/bin/php-cgi -c %s -f %s';
    const RENDER = 'cat %s';
    const PHP_INI = '/var/www/server/etc/php-cgi.ini';
    const HOST_DIR = '/var/www/server/etc/host';
}
