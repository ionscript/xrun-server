<?php


class HttpResponse extends HttpMessage implements HttpResponseInterface
{
    protected $status;

    public static function create($string)
    {
        $lines = explode("\r\n", $string);
        if (!is_array($lines) || count($lines) === 1) {
            $lines = explode("\n", $string);
        }

        $firstLine = array_shift($lines);

        $response = new static;

        $regex = '/^HTTP\/(?P<version>1\.[01]) (?P<status>\d{3})(?:[ ]+(?P<message>.*))?$/';
        $matches = [];
        if (!preg_match($regex, $firstLine, $matches)) {
            throw new \InvalidArgumentException(
                'A valid response status line was not found in the provided string'
            );
        }

        $response->setVersion($matches['version']);
        $response->setStatus($matches['status']);

        if (count($lines) === 0) {
            return $response;
        }

        $isHeader = true;
        $headers = $content = [];

        foreach ($lines as $line) {
            if ($isHeader && $line === '') {
                $isHeader = false;
                continue;
            }

            if ($isHeader) {
                if (preg_match("/[\r\n]/", $line)) {
                    throw new \RuntimeException('CRLF injection detected');
                }
                $headers[] = $line;
                continue;
            }

            if (!$content && preg_match('/^[a-z0-9!#$%&\'*+.^_`|~-]+:$/i', $line)
            ) {
                throw new \RuntimeException('CRLF injection detected');
            }

            $content[] = $line;
        }

        if ($headers) {
            $response->setHeaders(implode("\r\n", $headers));
        }

        if ($content) {
            $response->setContent(implode("\r\n", $content));
        }

        return $response;
    }

    public function getBody()
    {
        $body = (string)$this->getContent();

        $transferEncoding = $this->getHeaders()['Transfer-Encoding'];

        if (!empty($transferEncoding)) {
            if (strtolower($transferEncoding) === 'chunked') {
                $body = $this->decodeChunkedBody($body);
            }
        }

        $contentEncoding = $this->getHeaders()['Content-Encoding'];

        if (!empty($contentEncoding)) {
            if ($contentEncoding === 'gzip') {
                $body = $this->decodeGzip($body);
            } elseif ($contentEncoding === 'deflate') {
                $body = $this->decodeDeflate($body);
            }
        }

        return $body;
    }

    public function isClientError()
    {
        return $this->getStatus() < 500 && $this->getStatus() >= 400;
    }

    public function isForbidden()
    {
        return 403 === $this->getStatus();
    }

    public function isInformational()
    {
        return $this->getStatus() >= 100 && $this->getStatus() < 200;
    }

    public function isNotFound()
    {
        return 404 === $this->getStatus();
    }

    public function isOk()
    {
        return 200 === $this->getStatus();
    }

    public function isServerError()
    {
        return 500 <= $this->getStatus() && 600 > $this->getStatus();
    }

    public function isRedirect()
    {
        return 300 <= $this->getStatus() && 400 > $this->getStatus();
    }

    public function isSuccess()
    {
        return 200 <= $this->getStatus() && 300 > $this->getStatus();
    }

    public function renderStatusLine()
    {
        $status = sprintf(
            'HTTP/%s %s %s',
            $this->getVersion(),
            $this->getStatus(),
            $this->getStatusMessage()
        );

        return trim($status);
    }

    public function toString()
    {
        $str = $this->renderStatusLine() . "\r\n";
        $str .= $this->getHeaders();
        $str .= "\r\n";
        $str .= $this->getContent();
        return $str;
    }

    protected function decodeChunkedBody($body)
    {
        $decBody = '';

        while (trim($body)) {
            if (!preg_match("/^([\da-fA-F]+)[^\r\n]*\r\n/sm", $body, $m)) {
                throw new \RuntimeException(
                    "Error parsing body - doesn't seem to be a chunked message"
                );
            }

            $length = hexdec(trim($m[1]));
            $cut = strlen($m[0]);
            $decBody .= substr($body, $cut, $length);
            $body = substr($body, $cut + $length + 2);
        }

        return $decBody;
    }

    protected function decodeGzip($body)
    {
        if (!function_exists('gzinflate')) {
            throw new \RuntimeException(
                'zlib extension is required in order to decode "gzip" encoding'
            );
        }

        return gzinflate(substr($body, 10));
    }

    protected function decodeDeflate($body)
    {
        if (!function_exists('gzuncompress')) {
            throw new \RuntimeException(
                'zlib extension is required in order to decode "deflate" encoding'
            );
        }

        $zlibHeader = unpack('n', substr($body, 0, 2));

        if ($zlibHeader[1] % 31 === 0) {
            return gzuncompress($body);
        }

        return gzinflate($body);
    }

    public function setStatus($code)
    {
        $this->status = $code;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusMessage()
    {
        return self::STATUS[$this->status];
    }
}
