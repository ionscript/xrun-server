<?php

final class HttpServer extends SocketServer implements HttpServerInterface
{
    protected static $protocol = 'http';

    protected static $host;
    protected static $port;

    protected static $request;
    protected static $response;

    public function create($host = 'localhost')
    {
        static::$host = $host;
        parent::create();

        return $this;
    }

    public function listen($port = 80)
    {
        static::$port = $port;
        parent::listen();
        echo "Listen: " . static::$addr . ':' . static::$port . PHP_EOL;

        do {
            if (!$this->connection()) {
                break;
            }

            if ($bytes = $this->receive()) {

                echo "  Receive $bytes byte(s) from " . static::$remoteaddr . ':' . static::$remoteport . PHP_EOL;

                $sent =
                    $this
                        ->dispatch(
                            $this
                                ->output(
                                    HttpRequest::create(Buffer::$buffer)
                                )
                        );

                echo "  Send     $sent byte(s) to   " . static::$remoteaddr . ':' . static::$remoteport . PHP_EOL;
            }

            $this->closeConn();

        } while (true);

        $this->closeSock();
    }

    protected function dispatch($data)
    {
        if (file_exists($data['_SERVER']['SCRIPT_FILENAME'])) {
            if (strpos(mime_content_type($data['_SERVER']['SCRIPT_FILENAME']), 'x-') !== false) {
                return $this->execute($data['_SERVER']['SCRIPT_FILENAME'], $data);
            }

            return $this->render($data['_SERVER']['SCRIPT_FILENAME']);
        }

        return $this->send(404, '<html>404 PAGE NOT FOUND</html>');
    }

    protected function send($status, $conetnt)
    {
        $respone = new HttpResponse();

        $respone->setStatus($status)->setContent($conetnt);

        Buffer::$buffer = $respone->toString();

        return
            $this->write();
    }

    protected function render($filename)
    {
        $command = sprintf(
            static::RENDER,
            $filename
        );

        echo "      render: $filename\n";

        Buffer::$buffer = `$command`;
        return
            $this->write();
    }

    protected function execute($filename, $data)
    {
        $command = sprintf(
            static::EXECUTE,
            json_encode($data),
            static::PHP_INI,
            $filename
        );

        echo "      execute: $filename\n";

        Buffer::$buffer = `$command`;
        return
            $this->write();
    }

    protected function output(HttpRequest $request)
    {
        $SERVER_NAME = $request->getUri()->getHost();
        $DOCUMENT_ROOT = (string)!file_exists(static::HOST_DIR . '/' . $SERVER_NAME) ?: (string)readlink(static::HOST_DIR . '/' . $SERVER_NAME);
        $REQUEST_URI = $request->getUri()->getPath();
        $SCRIPT_NAME = $REQUEST_URI . (is_dir($REQUEST_URI) ? 'index.php' : '');
        $REQUEST_METHOD = $request->getMethod();
        $QUERY_STRING = $request->getUri()->getQuery();
        $REQUEST_SCHEME = $request->getUri()->getScheme();
        $SERVER_PROTOCOL = $request->getProtocol();
        $HEADERS = $request->getHeaders(true);

        return
            [
                '_SERVER' => array_merge([
                    'SERVER_PROTOCOL' => $SERVER_PROTOCOL,
                    'SERVER_NAME' => $SERVER_NAME,
                    'DOCUMENT_ROOT' => $DOCUMENT_ROOT,
                    'REQUEST_URI' => $REQUEST_URI,
                    'SCRIPT_NAME' => $SCRIPT_NAME,
                    'SCRIPT_FILENAME' => $DOCUMENT_ROOT . $SCRIPT_NAME,
                    'REQUEST_METHOD' => $REQUEST_METHOD,
                    'QUERY_STRING' => $QUERY_STRING ?: '',
                    'REQUEST_SCHEME' => $REQUEST_SCHEME
                ],
                    $HEADERS
                ),
                '_REQUEST' => [
                    $REQUEST_METHOD => $QUERY_STRING ?: '',
                ],
                '_' . $REQUEST_METHOD => $QUERY_STRING ?: '',
            ];
    }
}
