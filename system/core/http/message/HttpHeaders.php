<?php

class HttpHeaders
{
    protected $header = [];
    
    public static function create($string)
    {
        $headers = new static;
        $emptyLine = 0;

        foreach (explode("\r\n", $string) as $line) {

            if (preg_match('/^\s*$/', $line)) {
                ++$emptyLine;
                if ($emptyLine > 2) {
                    throw new \RuntimeException('Malformed header detected');
                }
                continue;
            }

            if ($emptyLine) {
                throw new \RuntimeException('Malformed header detected');
            }

            $parts = explode(':', $line, 2);

            $key = array_shift($parts);
            $value = static::filterValue(array_shift($parts));


            if (!preg_match('/^[!#$%&\'*+\-\.\^_`|~0-9a-zA-Z]+$/', $key)) {
                throw new \InvalidArgumentException(
                    'Header name is invalid.'
                );
            }

            if (!$value) {
                throw new \InvalidArgumentException('Header must match with the format "name:value"');
            }

            if (preg_match('/^\s+$/', $value)) {
                $value = '';
            }
            
            $headers->header[$key] = ltrim($value);
        }

        return $headers;
    }

    protected static function filterValue($value)
    {
        $string = '';
        foreach ((array)$value as $v) {
            $ascii = ord($v);

            if (($ascii < 32 && $ascii !== 9)
                || $ascii === 127
                || $ascii > 254
            ) {
                continue;
            }

            $string .= $v;
        }

        return $string;
    }

    public function addHeader(array $headers)
    {
        array_merge($this->header, $headers);

        return $this;
    }
    
    public function remove($key)
    {
        if (array_key_exists($key, $this->header)) {
            unset($this->header[$key]);

            return true;
        }
        return false;
    }
    
    public function clear()
    {
        $this->headers = [];
        return $this;
    }
    
    public function get($key)
    {
        if (!array_key_exists($key, $this->header)) {
            return false;
        }

        return $this->header[$key];
    }

    public function toString()
    {
        $headers = '';
        foreach ($this->toArray() as $key => $value) {
            $headers .= $key . ': ' . $value . "\r\n";
        }
        return $headers;
    }

    public function toArray()
    {
        $headers = [];

        foreach ($this->header as $header) {
                $matches = null;
                preg_match('/^(?P<key>[^()><@,;:\"\\/\[\]?=}{ \t]+):\s*(?P<value>.*)$/', $header['line'], $matches);
                if ($matches) {
                    $headers[$matches['key']] = $matches['value'];
                }

        }

        return $headers;
    }
}
