<?php


abstract class HttpMessage extends Message
{
    const VERSION_10 = '1.0';
    const VERSION_11 = '1.1';
    const VERSION_20 = '2.0';

    protected $version = self::VERSION_11;

    protected $headers;

    public function setVersion($version)
    {
        $this->version = $version;
        return $this;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }
    
    public function getHeaders($normalize = false)
    {
        if($normalize){
            $headers = [];

            foreach ($this->headers as $key => $value) {
                $headers['HTTP_' . strtoupper(str_replace('-', '_', $key))] = $value;
            }

            return $headers;
        }

        return $this->headers;
    }
}
