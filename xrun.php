<?php
set_include_path(
    get_include_path().PATH_SEPARATOR.
    __DIR__.'/system/core/http/'.PATH_SEPARATOR.
    __DIR__.'/system/core/http/message/'.PATH_SEPARATOR.
    __DIR__.'/system/core/http/response/'.PATH_SEPARATOR.
    __DIR__.'/system/core/http/request/'.PATH_SEPARATOR.
    __DIR__.'/system/core/message/'.PATH_SEPARATOR.
    __DIR__.'/system/core/process/'.PATH_SEPARATOR.
    __DIR__.'/system/core/socket/'.PATH_SEPARATOR.
    __DIR__.'/system/core/std/'.PATH_SEPARATOR.
    __DIR__.'/system/core/uri/'.PATH_SEPARATOR

);

spl_autoload_register(function ($class){
    //var_dump($class);
    include_once $class.'.php';
});

$s = new HttpServer();
$s->create()->listen(8080);

$d = true;
